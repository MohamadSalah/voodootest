package com.saos.salah.voodootest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Salah on 15/07/2018.
 */

public class PremiumActivity extends AppCompatActivity {
    @BindView(R.id.text_premium)
    protected TextView textPremium;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_premium);
        ButterKnife.bind(this);
    }
}