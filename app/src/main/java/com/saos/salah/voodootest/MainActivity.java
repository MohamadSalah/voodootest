package com.saos.salah.voodootest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.vidcoin.sdkandroid.VidCoin;
import com.vidcoin.sdkandroid.core.VidCoinBase;
import com.vidcoin.sdkandroid.core.interfaces.VidCoinCallBack;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements VidCoinCallBack {

    @BindView(R.id.button)
    protected Button buttonVideo;

    @BindView(R.id.text_no_available)
    protected TextView textNoAvailable;

    private final VidCoin mVidCoin = VidCoin.getInstance();

    @Override
    protected void onStart() {
        super.onStart();
        mVidCoin.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mVidCoin.init(this, this.getString(R.string.app_id), this);

        Map<VidCoinBase.VCUserInfos, String> userInfos = new HashMap<VidCoinBase.VCUserInfos, String>();

        userInfos.put(VidCoinBase.VCUserInfos.VC_USER_APP_ID, "UserName");

        mVidCoin.setUserInfos(userInfos);

        boolean videoIsAvailable = mVidCoin.videoIsAvailableForPlacement(this.getString(R.string.placement_code));
        if (videoIsAvailable) {
            buttonVideo.setVisibility(View.VISIBLE);
            textNoAvailable.setVisibility(View.INVISIBLE);
            buttonVideo.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View v)
                {
                    playVideo();
                }
            });
        }
        else {
            buttonVideo.setVisibility(View.INVISIBLE);
            textNoAvailable.setVisibility(View.VISIBLE);
        }
    }

    protected void playVideo() {
        mVidCoin.playAdForPlacement(this, this.getString(R.string.placement_code), -1);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mVidCoin.onStop();
    }

    @Override
    public void vidCoinCampaignsUpdate() {

    }

    @Override
    public void vidCoinViewWillAppear() {

    }

    @Override
    public void vidCoinViewDidDisappearWithViewInformation(HashMap<VidCoinBase.VCData, String> hashMap) {
        Intent intent = new Intent(this, PremiumActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void vidCoinDidValidateView(HashMap<VidCoinBase.VCData, String> hashMap) {

    }
}
